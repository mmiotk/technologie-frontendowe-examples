# Technologie Frontendowe Examples

Przykłady używane podczas przedmiotu Technologie Frontendowe prowadzone na PJATK Gdańsk

- Wykład 7 (JSX w React): [https://gitlab.com/mmiotk/technologie-frontendowe-examples/-/tree/lecture_7](https://gitlab.com/mmiotk/technologie-frontendowe-examples/-/tree/lecture_7)
- Wykład 8 (React state management): [https://gitlab.com/mmiotk/technologie-frontendowe-examples/-/tree/lecture_8](https://gitlab.com/mmiotk/technologie-frontendowe-examples/-/tree/lecture_8)